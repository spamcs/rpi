package util;

import java.sql.Timestamp;

import org.hibernate.Session;

import pojos.Inditasok;

public class InditasokUtil {

	
	public void addInditasMost() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        Inditasok s = new Inditasok(new Timestamp(System.currentTimeMillis()), 0);

        session.persist(s);
        session.getTransaction().commit();
        session.close();
        
        System.out.println(s.getId() + ":" + s.getIdo() + ":" + s.getSiker());

	}
	
}
