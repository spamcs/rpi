package pojos;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Inditasok {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	private long id;
	
	@Column
	private Timestamp ido;
	
	// 0 nem futott, 1.. alkalommal futott, -1.. hiba
	@Column
	private int siker;

	public Inditasok() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Inditasok(Timestamp ido, int siker) {
		this.ido = ido;
		this.siker = siker;
	}

	public Inditasok(long id, Timestamp ido, int siker) {
		this.id = id;
		this.ido = ido;
		this.siker = siker;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Timestamp getIdo() {
		return ido;
	}

	public void setIdo(Timestamp ido) {
		this.ido = ido;
	}

	public int getSiker() {
		return siker;
	}

	public void setSiker(int siker) {
		this.siker = siker;
	}
	
	

}
